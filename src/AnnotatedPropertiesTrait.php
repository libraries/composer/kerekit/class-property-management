<?php

namespace Kerekit\ClassPropertyManagement;

use Doctrine\Common\Annotations\{AnnotationReader,AnnotationRegistry};
use Kerekit\ClassPropertyManagement\Annotation\{Readable,Writable};

// Deprecated and will be removed in 2.0 but currently needed
AnnotationRegistry::registerLoader ('class_exists');

trait AnnotatedPropertiesTrait
{
    /**
     * @return $this when receiving a valid set{Property}
     * @throws \Error when trying to set inaccessible or undefined property
     */
    public function __call (string $method, array $arguments)
    {
        if (strpos ($method, 'set') === 0) {
            $propertyName = lcfirst (substr ($method, 3));
            if ($method === ('set' . ucfirst ($propertyName))) {
                $property = self::_getReflectionProperty ($propertyName);

                // Invalid number of arguments
                $argumentCount = count ($arguments);
                if ($argumentCount !== 1) {
                    throw new \Error ("Expected 1 argument, got $argumentCount.");
                }
                $value = $arguments [0];

                if (self::_isWritable ($property)) {
                    $this->{$propertyName} = $value;
                    return $this;
                }
            }
        }
        $msg = sprintf ('Call to undefined method %s::%s()',
            static::class,
            $method
        );
        throw new \Error ($msg);
    }

    /**
     * @return mixed|null NULL on undefined property.
     * @throws \Error on invisible properties.
     */
    public function __get (string $name)
    {
        $property = self::_getReflectionProperty ($name);
        if (is_null ($property)) {
            self::_triggerUndefinedPropertyNotice ($name);
            return;
        }

        // Annotated as Readable
        if (self::_isReadable ($property)) {
            return $this->{$name};
        }

        // Invisible
        throw self::_inaccessbileError ($property);
    }

    public function __isset (string $name): bool
    {
        $property = self::_getReflectionProperty ($name);
        if (is_null ($property)) {
            return false;
        }

        // Annotated as Readable
        if (self::_isReadable ($property)) {
            return isset ($this->{$name});
        }

        // Invisible
        return false;
    }

    public function __set (string $name, $value): void
    {
        $property = self::_getReflectionProperty ($name);
        if (is_null ($property)) {
            $this->{$name} = $value;
            return;
        }

        // Annotated as Writable
        if (self::_isWritable ($property)) {
            $this->{$name} = $value;
            return;
        }

        // Inaccessible
        throw self::_inaccessbileError ($property);
    }

    public function __unset (string $name): void
    {
        $property = self::_getReflectionProperty ($name);
        if (is_null ($property)) {
            return;
        }

        // Annotated as Writable
        if (self::_isWritable ($property)) {
            unset ($this->{$name});
            return;
        }

        // Inaccessible
        throw self::_inaccessbileError ($property);
    }

    private static function _getReflectionProperty (
        string $name
    ): ?\ReflectionProperty
    {
        $reflectionClass = new \ReflectionClass (static::class);
        try {
            return $reflectionClass->getProperty ($name);
        } catch (\ReflectionException $e) {
            return null;
        }
    }

    private static function _inaccessbileError (\ReflectionProperty $p): \Error
    {
        $msg = sprintf ('Cannot access %s property %s::$%s',
            $p->isPrivate () ? 'private' : 'protected',
            static::class,
            $p->getName ()
        );
        return new \Error ($msg);
    }

    private static function _isReadable (\ReflectionProperty $property): bool
    {
        $reader = new AnnotationReader;
        $annotation = $reader->getPropertyAnnotation (
            $property,
            Readable::class
        );
        return $annotation instanceof Readable;
    }

    private static function _isWritable (\ReflectionProperty $property): bool
    {
        $reader = new AnnotationReader;
        $annotation = $reader->getPropertyAnnotation (
            $property,
            Writable::class
        );
        return $annotation instanceof Writable;
    }

    /** @param string $name Property name */
    private static function _triggerUndefinedPropertyNotice (string $name): void
    {
        $msg = sprintf ('Undefined property: %s::$%s', static::class, $name);
        trigger_error ($msg);
    }
}
