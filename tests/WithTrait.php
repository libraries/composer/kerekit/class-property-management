<?php

namespace Kerekit\ClassPropertyManagement\Tests;

use Kerekit\ClassPropertyManagement\AnnotatedPropertiesTrait;
use Kerekit\ClassPropertyManagement\Annotation\{Readable,Writable};

class WithTrait extends WithoutTrait
{
    use AnnotatedPropertiesTrait;

    private $private = 'private';

    /** @Readable */
    private $r = 'r';

    /** @Readable @Writable */
    private $rw = 'rw';
}
