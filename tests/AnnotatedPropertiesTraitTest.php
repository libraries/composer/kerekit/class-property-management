<?php declare (strict_types = 1);

namespace Kerekit\ClassPropertyManagement\Tests;

use PHPUnit\Framework\Error\Notice;
use PHPUnit\Framework\TestCase;

final class AnnotatedPropertiesTraitTest extends TestCase
{
    static protected function _getErrorData (?\Throwable $e): array
    {
        if (is_null ($e)) {
            return [
                'class'   => null,
                'code'    => null,
                'message' => null,
            ];
        }
        return [
            'class'   => get_class ($e),
            'code'    => $e->getCode (),
            'message' => self::_strippedErrorMessage ($e->getMessage ()),
        ];
    }

    /** @return string Error message stripped from test class names */
    static private function _strippedErrorMessage (string $message): string
    {
        $searches = [WithoutTrait::class, WithTrait::class];
        return str_replace ($searches, '', $message);
    }

    public function testReadingUndefinedPropertyRaisesSameNotice (): void
    {

        // Init instances with & without trait
        $withoutTrait = new WithoutTrait;
        $withTrait    = new WithTrait;

        try {
            $x = $withTrait->undefined;
        } catch (Notice $actualNotice) {}
        try {
            $x = $withoutTrait->undefined;
        } catch (Notice $expectedNotice) {}
        $this->assertEquals (
            self::_strippedErrorMessage ($expectedNotice->getMessage ()),
            self::_strippedErrorMessage ($actualNotice->getMessage ())
        );
    }

    public function testSettingUndefinedPropertyWorks (): void
    {
        $instance = new WithTrait;
        $expected = 'bar';
        $instance->undefined = $expected;
        $this->assertEquals ($expected, $instance->undefined);
    }

    public function testReadingInaccessiblePropertyThrowsSameError (): void
    {
        // Init instances with & without trait
        $withoutTrait = new WithoutTrait;
        $withTrait    = new WithTrait;

        foreach (['private', 'protected'] as $property) {
            try {
                $x = $withTrait->{$property};
            } catch (\Error $actualError) {}
            try {
                $x = $withoutTrait->{$property};
            } catch (\Error $expectedError) {}
            $this->assertEquals (
                self::_getErrorData ($expectedError),
                self::_getErrorData ($actualError)
            );
        }
    }

    public function testSettingInaccessiblePropertyThrowsSameError (): void
    {
        // Init instances with & without trait
        $withoutTrait = new WithoutTrait;
        $withTrait    = new WithTrait;

        foreach (['private', 'protected'] as $property) {
            try {
                $withTrait->{$property} = '';
            } catch (\Error $actualError) {}
            try {
                $withoutTrait->{$property} = '';
            } catch (\Error $expectedError) {}
            $this->assertEquals (
                self::_getErrorData ($expectedError),
                self::_getErrorData ($actualError)
            );
        }
    }

    public function testSetMethodOnInaccessiblePropertyThrowsSameError (): void
    {
        // Init instances with & without trait
        $withoutTrait = new WithoutTrait;
        $withTrait    = new WithTrait;

        foreach (['private', 'protected'] as $property) {
            $method = 'set' . ucfirst ($property);
            try {
                $withTrait->{$method} ('');
            } catch (\Error $actualError) {}
            try {
                $withoutTrait->{$method} ('');
            } catch (\Error $expectedError) {}
            $this->assertEquals (
                self::_getErrorData ($expectedError),
                self::_getErrorData ($actualError)
            );
        }
    }

    public function testUnsettingInaccessiblePropertyThrowsSameError (): void
    {
        // Init instances with & without trait
        $withoutTrait = new WithoutTrait;
        $withTrait    = new WithTrait;

        foreach (['private', 'protected'] as $property) {
            try {
                unset ($withTrait->{$property});
            } catch (\Error $actualError) {}
            try {
                unset ($withoutTrait->{$property});
            } catch (\Error $expectedError) {}
            $this->assertEquals (
                self::_getErrorData ($expectedError),
                self::_getErrorData ($actualError)
            );
        }
    }

    /** @testdox __get() works on Readable property */
    public function test__getWorksOnReadableProperty (): void
    {
        $this->assertEquals ('r', (new WithTrait)->r);
    }

    /** @testdox __isset() works on Readable property */
    public function test__issetWorksOnReadableProperty (): void
    {
        $this->assertEquals (true, isset ((new WithTrait)->r));
    }

    /** @testdox __set() works on Writable property */
    public function test__setWorksOnWritableProperty (): void
    {
        $instance = new WithTrait;
        $expected = 'rw-updated';
        $instance->rw = $expected;
        $this->assertEquals ($expected, $instance->rw);
    }

    public function testSetMethodWorksOnWritableProperty (): void
    {
        $instance = new WithTrait;
        $expected = 'rw-updated';
        $instance->setRw ($expected);
        $this->assertEquals ($expected, $instance->rw);
    }

    /** @testdox __unset() works on Writable property */
    public function test__unsetWorksOnWritableProperty (): void
    {
        $instance = new WithTrait;
        unset ($instance->rw);
        $this->assertEquals (false, isset ($instance->rw));
    }
}
