# Class property management library

## Installation

Install the latest version with

```bash
$ composer require kerekit/class-property-management
```

## Usage

Use the `@Readable` and `@Writable` PHP doc-blocks to provide access to private
& protected properties.

```php
<?php

use Kerekit\ClassPropertyManagement\AnnotatedPropertiesTrait;
use Kerekit\ClassPropertyManagement\Annotation\{Readable,Writable};

class Entry
{
    use AnnotatedPropertiesTrait;

    /** @Readable */
    private $id = 1;

    /** @Readable @Writable */
    private $date = '2021-03-15';

    /** @Readable @Writable */
    private $text = '';
}

$entry = new Entry;

// Access Readable properties
$entry->id === 1;
var_dump (isset ($entry->id));

// Set Writable properties
$entry->date = '2021-03-16';

// Use chained set... methods for setting multiple Writable properties at once
$entry
    ->setDate ('2021-03-17')
    ->setText ('Some text here...')
    ;

// Unset Writable properties
unset ($entry->date, $entry->text);
```
