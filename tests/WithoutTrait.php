<?php

namespace Kerekit\ClassPropertyManagement\Tests;

class WithoutTrait
{
    private $private = 'private';
    protected $protected = 'protected';
}
